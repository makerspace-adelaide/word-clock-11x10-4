# Word Clock 

A 11 by 10 word clock that was originaly donated to Makerspace Adelaide in a broken condition. It's been repaired and the source code updated.
Originaly based on source from [here](https://bitbucket.org/tinkerelectric/word-clock/src/master/Electrical/code/code.ino)

```
   I T E I S F T L V N E 
   J Q U A R T E R C K O 
   T W E N T Y X F I V E 
   H A L F C T E N E T O 
   P A S T B S E V E N L
   O N E T W O T H R E E
   F O U R F I V E S I X
   N I N E K T W E L V E
   E I G H T E L E V E N
   T E N P Y O C L O C K
          • • • •
```

- Left button toggles DST time.
- Right button changes display colour.
- Both pressed puts it into test mode.

Parameters can be retreived (and set) through Particles api.
| Key      |   type   | Expected |
| ----     | -------- | ------   |
| `time`   | GET      | Unix time stamp |
| `DST`    | GET SET  | True or false depending on state. SET requires the first letter be `t` or `f` (capitals fine) |
| `colour` | GET SET  | Hex colour value, will ready any valid hex value send |
